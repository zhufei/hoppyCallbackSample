module Main where

import Foreign.Hoppy.Generator.Main (defaultMain)
import Foreign.Hoppy.Generator.Spec
import Foreign.Hoppy.Generator.Types

import Foreign.Hoppy.Generator.Std

cb_putStr :: Callback
cb_putStr = 
  makeCallback (toExtName "CallbackPutStrLn") [objT c_string] $ objT c_string 

moduleCallback :: Module 
moduleCallback = 
  moduleModify' (makeModule "moduleCallback" "genCallback.h" "genCallback.cpp") $
    moduleAddExports [ExportCallback cb_putStr]





fun_setCallBack :: Function
fun_setCallBack =
  addReqIncludes [includeLocal "callback.h"] $
    makeFn  (ident "setCallback")  (Just $ toExtName "setCallBack") Nonpure  [callbackT cb_putStr] voidT

moduleSetCallback :: Module
moduleSetCallback =
  moduleModify' (makeModule "moduleSetCallBack" "setCallback.h" "setCallback.cpp") $
    moduleAddExports [ ExportFn fun_setCallBack ]    



 
    
main :: IO ()
main = 
  defaultMain  (interface "interfaceCallback" [moduleCallback, mod_std] >>= interfaceAddHaskellModuleBase ["Sample", "Callback"])
  >>  
  defaultMain  (interface "interfaceSetCallback" [moduleSetCallback, moduleCallback, mod_std]    >>= interfaceAddHaskellModuleBase ["Sample", "ModuleSetCallback"])
