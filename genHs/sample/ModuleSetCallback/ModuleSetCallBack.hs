{-# LANGUAGE CPP, FlexibleContexts, FlexibleInstances, GeneralizedNewtypeDeriving, MultiParamTypeClasses, ScopedTypeVariables, TypeSynonymInstances, UndecidableInstances #-}
#if !MIN_VERSION_base(4,8,0)
{-# LANGUAGE OverlappingInstances #-}
#endif

---------- GENERATED FILE, EDITS WILL BE LOST ----------

module Sample.ModuleSetCallback.ModuleSetCallBack (
  setCallBack,
  ) where

import qualified Foreign as HoppyF
import qualified Foreign.Hoppy.Runtime as HoppyFHR
import Prelude ((>>=))
import qualified Prelude as HoppyP
import qualified Sample.ModuleSetCallback.ModuleCallback as M2
import qualified Sample.ModuleSetCallback.Std as M3

foreign import ccall "genpop__setCallBack" setCallBack' ::  HoppyFHR.CCallback (HoppyF.Ptr M3.StdStringConst -> HoppyP.IO (HoppyF.Ptr M3.StdStringConst)) -> HoppyP.IO ()

setCallBack ::  (HoppyP.String -> HoppyP.IO HoppyP.String) -> HoppyP.IO ()
setCallBack arg'1 =
  M2.callbackPutStrLn_new arg'1 >>= \arg'1' ->
  (setCallBack' arg'1')