import Distribution.Simple

import          Distribution.PackageDescription    hiding (Flag)   -- emptyHookedBuildInfo

import           Distribution.Simple.Setup                         -- configVerbosity

import           Distribution.Simple.Utils                         -- rawSystemExit

import           Distribution.Simple.BuildPaths                    -- getExeSourceFiles 

import           Distribution.Simple.LocalBuildInfo                -- LocalBuildInfo

import           System.FilePath                                   -- </>

import           System.Directory                                  -- getCurrentDirectory

import           System.Process                                    -- callProcess







copyExtLib :: Args -> CopyFlags -> PackageDescription -> LocalBuildInfo -> IO ()
copyExtLib _ flags pkg_descr lbi = do
    cwd <- getCurrentDirectory 
    let 
        progName = "demo"
        builddir = buildDir lbi
        progPath = cwd </> builddir </> progName </> "demo.exe"

    createDirectoryIfMissing True "bin"

    p <- doesFileExist progPath
 
    if p 
    then callProcess "cp"  [progPath, "./bin"]
    else putStrLn $ "---------------------" ++ progPath ++ " not exist"

    

















main = defaultMainWithHooks simpleUserHooks
    {
        -- preConf = makeExtLib ,
         postCopy = copyExtLib
    }
