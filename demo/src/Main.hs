module Main where
-- import Sample.Callback.ModuleCallback
-- import Sample.Callback.Std
import Sample.ModuleSetCallback.ModuleCallback
import Sample.ModuleSetCallback.ModuleSetCallBack
import Sample.ModuleSetCallback.Std


cb_hs :: String -> IO String
cb_hs s = do
  putStrLn $ s ++ " (i am in haskell function) "
  return "string returned from haskell"


main :: IO ()
main = do
  
  setCallBack cb_hs

  -- hscb <- callbackPutStrLn_new cb_hs
  -- setCallBack' hscb

  putStrLn "hello world, i am in demo"
