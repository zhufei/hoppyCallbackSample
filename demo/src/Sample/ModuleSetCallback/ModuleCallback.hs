{-# LANGUAGE CPP, FlexibleContexts, FlexibleInstances, GeneralizedNewtypeDeriving, MultiParamTypeClasses, ScopedTypeVariables, TypeSynonymInstances, UndecidableInstances #-}
#if !MIN_VERSION_base(4,8,0)
{-# LANGUAGE OverlappingInstances #-}
#endif

---------- GENERATED FILE, EDITS WILL BE LOST ----------

module Sample.ModuleSetCallback.ModuleCallback (
  callbackPutStrLn_newFunPtr,
  callbackPutStrLn_new,
  ) where

import qualified Foreign as HoppyF
import qualified Foreign.Hoppy.Runtime as HoppyFHR
import Prelude (($), (.), (=<<), (>>=))
import qualified Prelude as HoppyP
import qualified Sample.ModuleSetCallback.Std as M3

foreign import ccall "wrapper" callbackPutStrLn_new'newFunPtr :: (HoppyF.Ptr M3.StdStringConst -> HoppyP.IO (HoppyF.Ptr M3.StdStringConst)) -> HoppyP.IO (HoppyF.FunPtr (HoppyF.Ptr M3.StdStringConst -> HoppyP.IO (HoppyF.Ptr M3.StdStringConst)))
foreign import ccall "genpop__CallbackPutStrLn" callbackPutStrLn_new'newCallback :: HoppyF.FunPtr (HoppyF.Ptr M3.StdStringConst -> HoppyP.IO (HoppyF.Ptr M3.StdStringConst)) -> HoppyF.FunPtr (HoppyF.FunPtr (HoppyP.IO ()) -> HoppyP.IO ()) -> HoppyP.Bool -> HoppyP.IO (HoppyFHR.CCallback (HoppyF.Ptr M3.StdStringConst -> HoppyP.IO (HoppyF.Ptr M3.StdStringConst)))

callbackPutStrLn_newFunPtr :: (HoppyP.String -> HoppyP.IO HoppyP.String) -> HoppyP.IO (HoppyF.FunPtr (HoppyF.Ptr M3.StdStringConst -> HoppyP.IO (HoppyF.Ptr M3.StdStringConst)))
callbackPutStrLn_newFunPtr f'hs = callbackPutStrLn_new'newFunPtr $ \arg'1 ->
  HoppyFHR.decode (M3.StdStringConst arg'1) >>= \arg'1' ->
  (HoppyP.fmap (HoppyFHR.toPtr) . HoppyFHR.encode) =<<
  (f'hs arg'1')

callbackPutStrLn_new :: (HoppyP.String -> HoppyP.IO HoppyP.String) -> HoppyP.IO (HoppyFHR.CCallback (HoppyF.Ptr M3.StdStringConst -> HoppyP.IO (HoppyF.Ptr M3.StdStringConst)))
callbackPutStrLn_new f'hs = do
  f'p <- callbackPutStrLn_newFunPtr f'hs
  callbackPutStrLn_new'newCallback f'p HoppyFHR.freeHaskellFunPtrFunPtr HoppyP.False