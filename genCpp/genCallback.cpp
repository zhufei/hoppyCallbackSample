////////// GENERATED FILE, EDITS WILL BE LOST //////////

#include "genCallback.h"
#include <string>

extern "C" {

CallbackPutStrLn_impl::CallbackPutStrLn_impl(std::string const*(*f)(std::string const*), void (*release)(void(*)()), bool releaseRelease) :
    f_(f), release_(release), releaseRelease_(releaseRelease) {}

CallbackPutStrLn_impl::~CallbackPutStrLn_impl() {
    if (release_) {
        release_(reinterpret_cast<void(*)()>(f_));
        if (releaseRelease_) {
            release_(reinterpret_cast<void(*)()>(release_));
        }
    }
}

std::string CallbackPutStrLn_impl::operator()(std::string arg1_) {
std::string const*arg1 = &arg1_;
std::string const*resultPtr = f_(arg1);
std::string result = *resultPtr;
delete resultPtr;
return result;
}

std::string CallbackPutStrLn::operator()(std::string arg1) {
return (*impl_)(arg1);
}
CallbackPutStrLn::operator bool() const {
return static_cast<bool>(impl_);
}

CallbackPutStrLn_impl*genpop__CallbackPutStrLn(std::string const*(*f)(std::string const*), void(*release)(void(*)()), bool releaseRelease) {
return new CallbackPutStrLn_impl(f, release, releaseRelease);
}

}  // extern "C"
