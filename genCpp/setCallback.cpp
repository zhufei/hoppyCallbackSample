////////// GENERATED FILE, EDITS WILL BE LOST //////////

#include "callback.h"
#include "genCallback.h"
#include "setCallback.h"
#include <string>

extern "C" {

void genpop__setCallBack(CallbackPutStrLn_impl*arg1_) {
CallbackPutStrLn arg1(arg1_);
setCallback(arg1);
}

}  // extern "C"
