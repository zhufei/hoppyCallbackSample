////////// GENERATED FILE, EDITS WILL BE LOST //////////

#ifndef HOPPY_MODULE_interfaceSetCallback_moduleCallback
#define HOPPY_MODULE_interfaceSetCallback_moduleCallback

#include <memory>
#include <string>

extern "C" {

class CallbackPutStrLn_impl {
public:
    explicit CallbackPutStrLn_impl(std::string const*(*)(std::string const*), void(*)(void(*)()), bool);
    ~CallbackPutStrLn_impl();
    std::string operator()(std::string);
private:
    CallbackPutStrLn_impl(const CallbackPutStrLn_impl&);
    CallbackPutStrLn_impl& operator=(const CallbackPutStrLn_impl&);

    std::string const*(*const f_)(std::string const*);
    void (*const release_)(void(*)());
    const bool releaseRelease_;
};

class CallbackPutStrLn {
public:
    CallbackPutStrLn() {}
    explicit CallbackPutStrLn(CallbackPutStrLn_impl* impl) : impl_(impl) {}
    std::string operator()(std::string);
    operator bool() const;
private:
    std::shared_ptr<CallbackPutStrLn_impl> impl_;
};

}  // extern "C"

#endif  // ifndef HOPPY_MODULE_interfaceSetCallback_moduleCallback
